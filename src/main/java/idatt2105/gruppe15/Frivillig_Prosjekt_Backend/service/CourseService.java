package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    public List<Course> getAllCourses(String email) {
        List<Course> courses = new ArrayList<>();
        if (email == null) {
            courseRepository.findAll().forEach(courses::add);
        } else {
            courseRepository.findByStudentEmail(email).forEach(courses::add);
        }

//        for (int i = 0; i < courses.size(); i++) {
//            courses.get(i).setAssistants(courseRepository.findAssistantEmails(courses.get(i).getCourseId()));
//        }

        return courses;
    }

    public Course getCourseById(Long id) {
        Course course = courseRepository.findById(id);
        if (course != null) {
            course.setAssistants(courseRepository.findAssistantEmails(id));
        }
        return course;
    }

    public void createCourse(Course course) {
        courseRepository.save(course);
    }

    public boolean updateCourse(Long id, Course course) {
        Course _course = courseRepository.findById(id);
        if (_course != null) {
            _course.setCourseId(id);
            _course.setCourseCode(course.getCourseCode());
            _course.setCourseName(course.getCourseName());
            _course.setTotObligs(course.getTotObligs());
            _course.setRequiredObligs(course.getRequiredObligs());
            courseRepository.update(_course);
            return true;
        } else {
            return false;
        }
    }

    public int deleteCourse(Long id) {
        return courseRepository.deleteById(id);
    }

    public int deleteAllCourses() {
        return courseRepository.deleteAll();
    }

    public void addAssistant(Long id, String email) {
        courseRepository.addAssistant(id, email);
    }

    public int removeAssistant(Long id, String email) {
        return courseRepository.removeAssistant(id, email);
    }
}
