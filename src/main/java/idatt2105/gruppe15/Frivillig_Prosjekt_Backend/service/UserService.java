package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllUsers(Long id) {
        List<User> users = new ArrayList<>();
        if (id == null) {
            userRepository.findAll().forEach(users::add);
        } else {
            userRepository.findStudentsByCourseId(id).forEach(users::add);
        }
        return users;
    }

    public List<User> getAllAssistants(Long id) {
        List<User> assistants = new ArrayList<User>();
        if (id == null) {
            userRepository.findAllAssistants().forEach(assistants::add);
        } else {
            userRepository.findAssistantsByCourseId(id).forEach(assistants::add);
        }
        return assistants;
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public boolean updateUser(String email, User user) {
        User _user = userRepository.findByEmail(email);
        if (_user != null) {
            _user.setUserEmail(email);
            _user.setFirstName(user.getFirstName());
            _user.setLastName(user.getLastName());
            _user.setPassword(user.getPassword());
            _user.setAdmin(user.getAdmin());
            userRepository.update(_user);
            return true;
        } else {
            return false;
        }
    }

    public int deleteUser(String email) {
        return userRepository.deleteByEmail(email);
    }

    public int deleteAllUsers() {
        return userRepository.deleteAll();
    }

    public List<User> findAdmins() {
        return userRepository.findAdmins();
    }

    public List<User> findStudents() {
        return userRepository.findStudents();
    }
}
