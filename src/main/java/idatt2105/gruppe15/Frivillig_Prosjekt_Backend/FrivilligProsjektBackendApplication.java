package idatt2105.gruppe15.Frivillig_Prosjekt_Backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrivilligProsjektBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrivilligProsjektBackendApplication.class, args);
	}

}
