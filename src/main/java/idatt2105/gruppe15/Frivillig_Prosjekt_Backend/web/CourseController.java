package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/courses")
    public ResponseEntity<List<Course>> getAllCourses(@RequestParam(required = false) String email) {
        try {
            List<Course> courses = courseService.getAllCourses(email);
            if (courses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(courses, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/courses/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable("id") Long id) {
        Course course = courseService.getCourseById(id);
        if (course != null) {
            return new ResponseEntity<>(course, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/courses")
    public ResponseEntity<String> createCourse(@RequestBody Course course) {
        try {
            courseService.createCourse(course);
            return new ResponseEntity<>("Course was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/courses/{id}")
    public ResponseEntity<String> updateCourse(@PathVariable("id") Long id, @RequestBody Course course) {
        boolean response = courseService.updateCourse(id, course);
        if (response) {
            return new ResponseEntity<>("Course was updated successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot find course with id=" + id, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/courses/{id}")
    public ResponseEntity<String> deleteCourse(@PathVariable("id") Long id) {
        try {
            int result = courseService.deleteCourse(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find course with id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Course was deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete course.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/courses")
    public ResponseEntity<String> deleteAllCourses() {
        try {
            int numRows = courseService.deleteAllCourses();
            return new ResponseEntity<>("Deleted " + numRows + " course(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete courses.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/courses/{id}/assistants/add/{email}")
    public ResponseEntity<String> addAssistant(@PathVariable("id") Long id, @PathVariable("email") String email) {
        try {
            courseService.addAssistant(id, email);
            return new ResponseEntity<>("Assistant was added successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/courses/{id}/assistants/remove/{email}")
    public ResponseEntity<String> removeAssistant(@PathVariable("id") Long id, @PathVariable("email") String email) {
        try {
            int result = courseService.removeAssistant(id, email);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find assistant with email=" + email + " and course id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Assistant was removed successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot remove assistant.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
