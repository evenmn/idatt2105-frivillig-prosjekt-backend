package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

public class User {
    private String userEmail;
    private String firstName;
    private String lastName;
    private int password;
    private boolean admin;

    public User() {
    }

    public User(String userEmail, String firstName, String lastName, int password, boolean admin) {
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.admin = admin;
    }

    public User(String userEmail, String firstName, String lastName, int password) {
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        admin = false;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
