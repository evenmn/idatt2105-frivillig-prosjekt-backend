package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.List;

public class StudentCourse {

    private String studentEmail;
    private String CourseCode;
    private List<Integer> obligs;

    public StudentCourse(String studentEmail, String courseCode) {
        this.studentEmail = studentEmail;
        CourseCode = courseCode;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getCourseCode() {
        return CourseCode;
    }

    public void setCourseCode(String courseCode) {
        CourseCode = courseCode;
    }

    public List<Integer> getObligs() {
        return obligs;
    }

    public void setObligs(List<Integer> obligs) {
        this.obligs = obligs;
    }
}
