package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.List;

public class Course {

    private long courseId;
    private String courseCode;
    private String courseName;
    private int totObligs;
    private int requiredObligs;
    private List<String> assistants;

    public Course() {
    }

    public Course(long courseId, String courseCode, String courseName, int totObligs, int requiredObligs) {
        this.courseId = courseId;
        this.courseCode = courseCode;
        this.courseName = courseName;
        this.totObligs = totObligs;
        this.requiredObligs = requiredObligs;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getTotObligs() {
        return totObligs;
    }

    public void setTotObligs(int totObligs) {
        this.totObligs = totObligs;
    }

    public int getRequiredObligs() {
        return requiredObligs;
    }

    public void setRequiredObligs(int requiredObligs) {
        this.requiredObligs = requiredObligs;
    }

    public List<String> getAssistants() {
        return assistants;
    }

    public void setAssistants(List<String> assistants) {
        this.assistants = assistants;
    }
}
