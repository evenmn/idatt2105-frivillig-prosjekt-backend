package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.Date;

public class QueueElement {

    private long queueId;
    private String userEmail;
    private String courseCode;
    private int obligNr;
    private Date date;
    private String building;
    private String room;
    private int table;
    private String message;
    private String assistant;

    public QueueElement(long queueId, String userEmail, String courseCode, int obligNr,
                        Date date, String building, String room, int table, String message,
                        String assistant) {
        this.queueId = queueId;
        this.userEmail = userEmail;
        this.courseCode = courseCode;
        this.obligNr = obligNr;
        this.date = date;
        this.building = building;
        this.room = room;
        this.table = table;
        this.message = message;
        this.assistant = assistant;
    }

    public long getQueueId() {
        return queueId;
    }

    public void setQueueId(long queueId) {
        this.queueId = queueId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getObligNr() {
        return obligNr;
    }

    public void setObligNr(int obligNr) {
        this.obligNr = obligNr;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }
}
