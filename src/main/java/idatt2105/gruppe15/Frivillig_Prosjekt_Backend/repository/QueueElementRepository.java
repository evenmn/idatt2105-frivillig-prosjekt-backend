package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;

import java.util.List;

public interface QueueElementRepository {

    int save(QueueElement queueElement);
    int update(QueueElement queueElement);
    QueueElement findById(long id);
    int deleteById(long id);
    List<QueueElement> findAll();
    List<QueueElement> findByCourseCode(String courseCode);
    int beingAssisted(String assistantEmail);
    int deleteAll();
}
