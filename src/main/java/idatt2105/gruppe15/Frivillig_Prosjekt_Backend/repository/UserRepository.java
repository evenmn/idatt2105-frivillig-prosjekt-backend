package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;

import java.util.List;

public interface UserRepository {

    int save(User user);
    int update(User user);
    User findByEmail(String email);
    int deleteByEmail(String email);
    List<User> findAll();
    List<User> findAllAssistants();
    List<User> findStudentsByCourseId(long id);
    List<User> findAssistantsByCourseId(long id);
    List<User> findAdmins();
    List<User> findStudents();
    int deleteAll();
}
