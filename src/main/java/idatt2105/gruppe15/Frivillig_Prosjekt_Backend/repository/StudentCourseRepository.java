package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;

import java.util.List;

public interface StudentCourseRepository {

    int save(StudentCourse studentCourse);
    int update(StudentCourse studentCourse);
    StudentCourse findByStudentCourse(String userEmail, String courseCode);
    int deleteByStudentCourse(String userEmail, String courseCode);
    List<StudentCourse> findAll();
    List<Integer> findObligs(String userEmail, String courseCode);
    int approveOblig(String userEmail, String courseCode, int obligNr);
    int deleteAll();
}
