package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;

import java.util.List;

public interface CourseRepository {

    int save(Course course);
    int update(Course course);
    Course findById(long courseId);
    int deleteById(long courseId);
    List<Course> findAll();
    List<Course> findByStudentEmail(String email);
    List<String> findAssistantEmails(long courseId);
    int addAssistant(long courseId, String userEmail);
    int removeAssistant(long courseId, String userEmail);
    int deleteAll();

}
