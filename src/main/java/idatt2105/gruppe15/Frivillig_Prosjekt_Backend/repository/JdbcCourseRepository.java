package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcCourseRepository implements CourseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public int save(Course course) {
        return jdbcTemplate.update("INSERT INTO courses (courseCode, courseName, totObligs, requiredObligs) VALUES (?,?,?,?)",
                course.getCourseCode(), course.getCourseName(), course.getTotObligs(), course.getRequiredObligs());
    }

    @Override
    public int update(Course course) {
        return jdbcTemplate.update("UPDATE users SET courseCode=?, courseName=?, totObligs=?, requiredObligs=? WHERE courseId=?",
                course.getCourseCode(), course.getCourseName(), course.getTotObligs(), course.getRequiredObligs(), course.getCourseId());
    }

    @Override
    public Course findById(long courseId) {
        try {
            Course course = jdbcTemplate.queryForObject("SELECT * FROM courses WHERE courseId=?",
                    BeanPropertyRowMapper.newInstance(Course.class), courseId);
            return course;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteById(long courseId) {
        return jdbcTemplate.update("DELETE FROM courses WHERE courseId=?", courseId);
    }

    @Override
    public List<Course> findAll() {
        return jdbcTemplate.query("SELECT * FROM courses", BeanPropertyRowMapper.newInstance(Course.class));
    }

    @Override
    public List<Course> findByStudentEmail(String email) {
        return jdbcTemplate.query("SELECT * FROM courses JOIN users_courses ON (courses.courseId = users_courses.courseId) " +
                        "WHERE users_courses.userEmail = ?", BeanPropertyRowMapper.newInstance(Course.class), email);
    }

    @Override
    public List<String> findAssistantEmails(long courseId) {
        return jdbcTemplate.query("SELECT userEmail FROM users_courses WHERE courseId=?",
                BeanPropertyRowMapper.newInstance(String.class), courseId);
    }

    @Override
    public int addAssistant(long courseId, String userEmail) {
        return jdbcTemplate.update("INSERT INTO assistants (userEmail, courseId) VALUES (?,?)", userEmail, courseId);
    }

    @Override
    public int removeAssistant(long courseId, String userEmail) {
        return jdbcTemplate.update("DELETE FROM assistants WHERE userEmail=? AND courseId=?", userEmail, courseId);
    }

    @Override
    public int deleteAll() {
        return jdbcTemplate.update("DELETE FROM courses");
    }
}
